terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.36.0"
    }
  }
}

resource "digitalocean_ssh_key" "default" {
  name = "Terraform Example"
  public_key = file("~/.ssh/id_ed25519.pub")
}

resource "digitalocean_droplet" "test-srvos" {
  image    = "ubuntu-20-04-x64"
  name     = "machine1"
  region   = "tor1"
  size     = "s-1vcpu-2gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
}

module "system-build" {
  source    = "github.com/nix-community/nixos-anywhere//terraform/nix-build"
  attribute = ".#nixosConfigurations.machine1.config.system.build.toplevel"
}

module "disko" {
  source    = "github.com/nix-community/nixos-anywhere//terraform/nix-build"
  attribute = ".#nixosConfigurations.machine1.config.system.build.diskoScript"
}

module "deploy" {
  source                 = "github.com/nix-community/nixos-anywhere//terraform/all-in-one"
  nixos_system_attr      = ".#nixosConfigurations.machine1.config.system.build.toplevel"
  nixos_partitioner_attr = ".#nixosConfigurations.machine1.config.system.build.diskoScript"
  target_host            = resource.digitalocean_droplet.test-srvos.ipv4_address
  instance_id            = resource.digitalocean_droplet.test-srvos.ipv4_address

  depends_on = [
    digitalocean_droplet.test-srvos
  ]
}
