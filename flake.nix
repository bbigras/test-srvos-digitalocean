{
  description = "test-srvos-digitalocean";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=3f71d1bc02b6100222e440a6ad0571d8e0db4896";
    flake-parts = {
      url = "flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    srvos = {
      url = "github:nix-community/srvos?ref=ecf8f8dc0efe5479027a6269aefa09b646470be2";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ flake-parts, nixpkgs, disko, srvos, ... }: flake-parts.lib.mkFlake { inherit inputs; }
    {
      systems = [
        "x86_64-linux"
      ];

      perSystem = { pkgs, system, ... }:
        {
          devShells.default = pkgs.mkShell {
            buildInputs = [
              pkgs.opentofu
            ];
          };
        };
    } // {
    nixosConfigurations = {
      machine1 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          disko.nixosModules.disko
          { disko.devices.disk.disk1.device = "/dev/vda"; }
          ./configuration.nix
          srvos.nixosModules.hardware-digitalocean-droplet
        ];
      };
    };
  };
}
