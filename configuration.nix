{ lib, pkgs, ... }: {
  imports = [
    ./disk-config.nix
  ];
  boot.loader.grub = {
    # no need to set devices, disko will add all devices that have a EF02 partition to the list already
    # devices = [ ];
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  security.sudo.wheelNeedsPassword = lib.mkDefault false;
  services.openssh.enable = true;

  users.users.root.openssh.authorizedKeys.keys = [
  ];

  users.users.my_user = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    hashedPassword = "";
    openssh.authorizedKeys.keys = [
    ];
  };

  system.stateVersion = "23.11";
}
